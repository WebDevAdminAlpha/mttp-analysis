# frozen_string_literal: true
# rubocop: disable all
#
# This script displays a histogram that presents the time to production in
# buckets of 5 hours.

require 'json'
require 'time'

def to_hours(seconds)
  seconds / 3600.0
end

buckets = {}
max_bucket = 200
bucket_size = 10

(0..max_bucket).step(bucket_size).each do |step|
  next if step.zero?

  buckets[(step - bucket_size)..step] = 0
end

buckets[max_bucket..1000] = 0

Dir['./data/pipelines/*.json'].each do |path|
  data = JSON.load(File.read(path))
  finished_at = Time.parse(data['deployer']['finished_at'])

  data['deployer']['merge_requests'].each do |mr|
    time = to_hours(finished_at - Time.parse(mr['merged_at']))

    buckets.each do |bucket, _|
      next unless bucket.cover?(time)

      buckets[bucket] += 1
      break
    end
  end
end

total = buckets.values.sum.to_f

buckets.each do |bucket, count|
  next if count.zero?

  percentage = ((count.to_f / total) * 100).ceil
  blocks = '▮' * percentage
  label = "#{bucket}h".ljust(12, ' ')

  puts "#{label} #{blocks} #{count}"
end
