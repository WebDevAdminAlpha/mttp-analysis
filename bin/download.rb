# frozen_string_literal: true
# rubocop: disable all
#
# This script downloads the necessary data and stores it in JSON. Existing data
# files are ignored, making it possible to re-run this script multiple times
# without having to re-download everything.
#
# This script requires API tokens for GitLab.com, ops.gitlab.net, and
# dev.gitlab.org.

require 'addressable/uri'
require 'etc'
require 'gitlab'
require 'json'
require 'parallel'
require 'time'
require 'retriable'

CORES = Etc.nprocessors

RELEASE_TOOLS_USE_COM = ENV['RELEASE_TOOLS_USE_COM'].to_s == 'true'

# The path of the Release Tools project on the ops instance.
RELEASE_TOOLS_PROJECT = 'gitlab-org/release/tools'

# The path of the Release Tools project on GitLab.com.
RELEASE_TOOLS_PROJECT_COM = 'gitlab-org/release-tools'

# The path of the Omnibus project on dev.
OMNIBUS_GITLAB_PROJECT = 'gitlab/omnibus-gitlab'

# The path of the deployer project on the ops instance.
DEPLOYER_PROJECT = 'gitlab-com/gl-infra/deployer'

# The name of the Omnibus job that uploads the package that we will auto-deploy.
UPLOAD_JOB_NAME = 'Ubuntu-16.04-upload-deploy'

OPS_HOST = 'ops.gitlab.net'
COM_HOST = 'gitlab.com'

COM_CLIENT = Gitlab::Client.new(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV.fetch('COM_TOKEN')
)

OPS_CLIENT = Gitlab::Client.new(
  endpoint: 'https://ops.gitlab.net/api/v4',
  private_token: ENV.fetch('OPS_TOKEN')
)

DEV_CLIENT = Gitlab::Client.new(
  endpoint: 'https://dev.gitlab.org/api/v4',
  private_token: ENV.fetch('DEV_TOKEN')
)

def release_tools_client
  if RELEASE_TOOLS_USE_COM
    COM_CLIENT
  else
    OPS_CLIENT
  end
end

def release_tools_project
  if RELEASE_TOOLS_USE_COM
    RELEASE_TOOLS_PROJECT_COM
  else
    RELEASE_TOOLS_PROJECT
  end
end

def auto_paginate_with_retry(page)
  rows = []

  while page
    rows.concat(page.to_a)

    page = Retriable.retriable { page.next_page }
  end

  rows
end

def process_job(tag_job)
  path_prefix =
    if RELEASE_TOOLS_USE_COM
      COM_HOST
    else
      OPS_HOST
    end

  path =
    File.join(Dir.pwd, 'data', 'pipelines', "#{path_prefix}.#{tag_job.id}.json")

  return if File.file?(path)

  tag_log = Retriable.retriable do
    release_tools_client.job_trace(release_tools_project, tag_job.id)
  end

  tag_name = tag_log.match(/Creating Omnibus tag -- {:name=>"([^"]+)"/)&.[](1)

  if tag_name.nil?
    # Older builds use a different format for the logs.
    tag_name = tag_log.match(
      /Creating project tag -- {:project=>gitlab-org\/omnibus-gitlab, :name=>"([^"]+)"}/
    )&.[](1)
  end

  return unless tag_name

  tag_pipeline =
    release_tools_client.pipeline(release_tools_project, tag_job.pipeline.id)

  omnibus_tag = DEV_CLIENT.tag(OMNIBUS_GITLAB_PROJECT, tag_name)

  upload_job = DEV_CLIENT
    .commit_status(
      OMNIBUS_GITLAB_PROJECT,
      omnibus_tag.commit.id,
      name: UPLOAD_JOB_NAME
    )
    .auto_paginate
    .find { |job| job.status == 'success' }

  return unless upload_job

  # The upload job returned by the previous API call does not include all the
  # job details that we need, so we need to get those using a separate API call.
  upload_job = DEV_CLIENT.job(OMNIBUS_GITLAB_PROJECT, upload_job.id)
  upload_log = DEV_CLIENT.job_trace(OMNIBUS_GITLAB_PROJECT, upload_job.id)
  deploy_link = upload_log.match(/Deployer build triggered at ([^ ]+)/)&.[](1)

  return unless deploy_link

  upload_pipeline =
    DEV_CLIENT.pipeline(OMNIBUS_GITLAB_PROJECT, upload_job.pipeline.id)

  deploy_pipeline_id = deploy_link.split('/').last
  deploy_pipeline = Retriable.retriable do
    OPS_CLIENT.pipeline(DEPLOYER_PROJECT, deploy_pipeline_id)
  end

  return if deploy_pipeline.status == 'running'

  deploy_jobs = Retriable.retriable do
    OPS_CLIENT
      .pipeline_jobs(
        DEPLOYER_PROJECT,
        deploy_pipeline_id,
        scope: %w[success failed]
      )
      .auto_paginate
  end

  associated = Parallel
    .map(deploy_jobs, in_threads: CORES) do |job|
      url = Retriable
        .retriable do
          OPS_CLIENT.job_trace(DEPLOYER_PROJECT, job.id)
        end
        .match(/Triggered downstream pipeline -- {:web_url=>"([^"]+)"/)&.[](1)

      next unless url

      project, pipeline_id = Addressable::URI
        .parse(url)
        .path
        .split('/pipelines/')

      project_client =
        if url.include?(OPS_HOST)
          OPS_CLIENT
        else
          COM_CLIENT
        end

      [
        job,
        Retriable.retriable do
          project_client.pipeline(project[1..], pipeline_id)
        end
      ]
    end
    .compact

  deployer_finished_at = deploy_pipeline.finished_at ||
    (Time.parse(deploy_pipeline.started_at) + deploy_pipeline.duration).iso8601

  for_json = {
    release_tools: {
      id: tag_pipeline.id,
      url: tag_pipeline.web_url,
      tag: omnibus_tag.name,
      started_at: tag_pipeline.started_at,
      finished_at: tag_pipeline.finished_at,
      duration: tag_pipeline.duration
    },
    omnibus: {
      id: upload_pipeline.id,
      url: upload_pipeline.web_url,
      started_at: upload_pipeline.started_at,
      finished_at: upload_pipeline.finished_at,
      duration: upload_pipeline.duration
    },
    deployer: {
      id: deploy_pipeline.id,
      url: deploy_pipeline.web_url,
      status: deploy_pipeline.status,
      started_at: deploy_pipeline.started_at,
      finished_at: deployer_finished_at,
      duration: deploy_pipeline.duration,
      jobs: deploy_jobs.map do |job|
        {
          id: job.id,
          name: job.name,
          stage: job.stage,
          started_at: job.started_at,
          finished_at: job.finished_at,
          duration: job.duration,
        }
      end,
      pipelines: associated.each_with_object({}) do |(job, pipeline), hash|
        hash[job.name] = {
          id: pipeline.id,
          url: pipeline.web_url,
          started_at: pipeline.started_at,
          finished_at: pipeline.finished_at,
          duration: pipeline.duration,
        }
      end,
      merge_requests: []
    }
  }

  if (gprd_track = for_json[:deployer][:pipelines]['gprd-track'])
    if gprd_track[:url].include?(OPS_HOST)
      track_client = OPS_CLIENT
      track_project = RELEASE_TOOLS_PROJECT
    else
      track_client = COM_CLIENT
      track_project = RELEASE_TOOLS_PROJECT_COM
    end

    track_pipeline = Retriable.retriable do
      track_client.pipeline(track_project, gprd_track[:id])
    end

    track_job =
      auto_paginate_with_retry(
        track_client.pipeline_jobs(track_project, track_pipeline.id)
      )
      .find { |job| job.name == 'track-deployment' }

    track_trace = Retriable.retriable do
      track_client.job_trace(track_project, track_job.id)
    end

    for_json[:deployer][:merge_requests] = Parallel.map(
      track_trace.scan(/Adding label "workflow::production" to merge request (.+)/),
      in_threads: CORES
    ) do |matches|
      project, iid = Addressable::URI
        .parse(matches[0])
        .path
        .split('/-/merge_requests/')

      mr = Retriable.retriable { COM_CLIENT.merge_request(project[1..], iid) }

      {
        id: mr.iid,
        title: mr.title,
        url: mr.web_url,
        created_at: mr.created_at,
        merged_at: mr.merged_at || mr.updated_at,
        source_project_id: mr.source_project_id,
        target_project_id: mr.target_project_id,
        merge_commit_sha: mr.merge_commit_sha
      }
    end
  end

  File.open(path, 'w') do |file|
    file.write(JSON.pretty_generate(for_json))
  end
end

def process_jobs(jobs)
  Parallel.each(jobs, in_processes: CORES) do |job|
    process_job(job)
  end

  jobs.clear
end

min_time = (Date.today - 90).to_time
tag_jobs = []

release_tools_client
  .jobs(release_tools_project, scope: 'success', per_page: 100)
  .auto_paginate do |job|
    next unless job.name == 'auto_deploy:tag'
    break if Time.parse(job.started_at) <= min_time

    tag_jobs << job
  end

process_jobs(tag_jobs)
